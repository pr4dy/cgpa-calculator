export interface Subject {
    name: string | undefined,
    grade: string,
    credit: number
}