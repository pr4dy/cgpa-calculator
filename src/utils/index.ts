export const gradePoint = new Map(
    Object.entries({
        O: 10,
        "A+": 9,
        A: 8,
        "B+": 7,
        B: 6,
        U: 0,
        AB: 0,
        WD: 0,
    })
);

export const grades = ["O","A+","A","B+","B","U","AB","WD"]
export const credits = [4,3.5,3,2.5,2,1.5,1,0]